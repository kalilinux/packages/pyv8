#!/bin/sh

set -e

test_install_python2() {

    echo "Testing python2 package"
    for py in $(pyversions -r 2>/dev/null) ; do
	cd "$AUTOPKGTEST_TMP" ;
	echo "Testing with $py:" ;
	$py -c "import PyV8; print(PyV8)" ;
    done

}

###################################
# Main
###################################

for function in "$@"; do
        $function
done
